/**
 * Copyright (C) 2014 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package net.slavitica.slaviticacraft;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import net.slavitica.slaviticacraft.client.AuthenticationException;
import net.slavitica.slaviticacraft.client.controller.ConsoleController;
import net.slavitica.slaviticacraft.client.controller.LoginController;
import net.slavitica.slaviticacraft.client.controller.RootController;
import net.slavitica.slaviticacraft.client.model.ClientModel;
import net.slavitica.slaviticacraft.client.view.ConsoleView;
import net.slavitica.slaviticacraft.client.view.LoginView;
import net.slavitica.slaviticacraft.client.view.RootView;

public class App {

    private static final Logger LOGGER = Logger.getLogger(App.class.getSimpleName());
    public static final boolean DEBUG;

    static {
        DEBUG = true;
    }

    public static void main(String[] args) throws AuthenticationException, IOException, ParseException {
        Handler handler = new FileHandler("log.txt");
        SimpleFormatter formatter = new SimpleFormatter();
        handler.setFormatter(formatter);
        Logger.getLogger("").addHandler(handler);

        boolean baseDirectoryFlag = false;
        String baseDirectoryString = null;

        if (args.length > 0) {

            for (int i = 0; i < args.length; i++) {
                String arg = args[i];
                if (arg.equals("--baseDirectory") || arg.equals("-b")) {
                    baseDirectoryFlag = true;
                    baseDirectoryString = args[++i];
                } else {
                    throw new RuntimeException("Dont know arg:" + arg);
                }
            }
        }

        try {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e) {
        }
        String slaviticaCraftVersion = "0.0.12-SNAPSHOT";

        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "Running SlaviticaCraft Client: {0} Debug: {1}", new Object[]{slaviticaCraftVersion, DEBUG});
        }

        ClientModel model = new ClientModel();
        File baseDirectory;
        if(baseDirectoryFlag) {
            baseDirectory = new File(baseDirectoryString);
        } else {
            baseDirectory = new File(".");
        }
        model.setBaseDirectory(baseDirectory);
        model.loadConfigurations();

        final RootController rootController = new RootController(model);
        final RootView rootWindow = new RootView(rootController, "SlaviticaCraft Console (" + slaviticaCraftVersion + ")");
        rootWindow.setSize(800, 600);
        rootWindow.setLocation(300, 200);
        rootWindow.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                rootController.handleClose();
                System.exit(0);
            }
        });

        LoginController loginController = new LoginController(model);
        LoginView loginView = new LoginView(loginController);

        ConsoleController consoleController = new ConsoleController(model);
        ConsoleView consoleView = new ConsoleView(consoleController);

        rootWindow.addTab("Login", loginView);
        rootWindow.addTab("Console", consoleView);

        rootWindow.setVisible(true);
    }
}
