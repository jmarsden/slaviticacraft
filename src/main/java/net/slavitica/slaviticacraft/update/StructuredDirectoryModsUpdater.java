/**
 * Copyright (C) 2014 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package net.slavitica.slaviticacraft.update;

import cc.plural.jsonij.JPath;
import cc.plural.jsonij.JSON;
import cc.plural.jsonij.Value;
import cc.plural.jsonij.marshal.JSONMarshaler;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;

public class StructuredDirectoryModsUpdater extends ModsUpdater {

    String minecraftVersion;
    File sourceModsBaseDirectory;
    File destinationModsDirectory;
    Map<String, ModMeta> modMetaMap;
    private List<File> deleteList;
    private List<File> copyList;

    public StructuredDirectoryModsUpdater(String minecraftVersion, File baseModsDirectory, File instanceModDirectory) {
        super();
        this.minecraftVersion = minecraftVersion;
        this.sourceModsBaseDirectory = baseModsDirectory;
        this.destinationModsDirectory = instanceModDirectory;
    }

    @Override
    protected void handleInit() throws UpdateException {
        if (!sourceModsBaseDirectory.exists()) {
            throw new UpdateException("Base Directory Does Not Exist:" + sourceModsBaseDirectory.getAbsolutePath());
        }
        File modIndexFile = new File(sourceModsBaseDirectory, "mods.json");
        if (!modIndexFile.exists()) {
            throw new UpdateException("No Mods Index:" + modIndexFile.getAbsolutePath());
        }

        this.modMetaMap = new HashMap<String, ModMeta>();
        FileInputStream modFolderConfigFile = null;
        try {
            FileInputStream modConfigFile = new FileInputStream(modIndexFile);
            String[] mods = (String[]) JSONMarshaler.marshalJSON(modConfigFile, String[].class);

            for (String mod : mods) {
                System.out.println("Parsing Meta for Mod: " + mod);

                File modFolder = new File(sourceModsBaseDirectory, mod);
                File modFolderIndex = new File(modFolder, "mod.json");

                modFolderConfigFile = new FileInputStream(modFolderIndex);

                JSON modConfigJson = JSON.parse(modFolderConfigFile);

                String name = JPath.evaluate(modConfigJson, "name").getString();
                boolean clientSide = JPath.evaluate(modConfigJson, "clientside").getBoolean();
                boolean serverSide = JPath.evaluate(modConfigJson, "serverside").getBoolean();

                Map<String, String> versionMap = new HashMap<String, String>();
                Value versions = JPath.evaluate(modConfigJson, "versions");
                if (versions != null) {
                    Iterator<CharSequence> keySetIterator = versions.valueKeySet().iterator();
                    while (keySetIterator.hasNext()) {
                        String version = keySetIterator.next().toString();
                        String file = JPath.evaluate(versions, version + "/file").toString();
                        versionMap.put(version, file);
                    }
                }
                Map<String, String[]> modVersionMap = new HashMap<String, String[]>();
                Value modVersion = JPath.evaluate(modConfigJson, "modversions");
                if (modVersion != null) {
                    Iterator<CharSequence> keySetIterator = modVersion.valueKeySet().iterator();
                    while (keySetIterator.hasNext()) {
                        String version = keySetIterator.next().toString();
                        Value versionArrayValue = modVersion.get(version);
                        String[] versionArray = new String[versionArrayValue.size()];
                        for (int i = 0; i < versionArrayValue.size(); i++) {
                            versionArray[i] = versionArrayValue.get(i).toString();
                        }
                        modVersionMap.put(version, versionArray);
                    }
                }
                ModMeta modMeta = new ModMeta(name, clientSide, serverSide, versionMap, modVersionMap);
                modMetaMap.put(mod, modMeta);
            }
        } catch (Exception e) {
            throw new UpdateException(e);
        } finally {
            if (modFolderConfigFile != null) {
                try {
                    modFolderConfigFile.close();
                } catch (IOException ex) {}
            }
        }
    }

    @Override
    protected MOD_VALIDATION handleValidate() throws UpdateException {
        MOD_VALIDATION result = MOD_VALIDATION.NO_UPDATE;
        deleteList = new ArrayList<File>();
        copyList = new ArrayList<File>();

        List<File> modFolderContents = new ArrayList<File>();
        for (File modFile : destinationModsDirectory.listFiles()) {
            if (!modFile.isDirectory()) {
                modFolderContents.add(modFile);
            }
        }

        for (ModMeta modMeta : modMetaMap.values()) {

            if (mode != UPDATE_MODE.CLIENT_AND_SERVER && !(mode == UPDATE_MODE.CLIENT && modMeta.isClientSide()) && !(mode == UPDATE_MODE.SERVER && modMeta.isServerSide())) {
                continue;
            }

            String[] modVersions = modMeta.getModVersionMap().get(minecraftVersion);
            String version = null;
            if (modVersions != null && modVersions.length > 0) {
                version = modVersions[0];
            } else {
                throw new UpdateException("No Mod Version of " + modMeta.getName() + ":" + minecraftVersion);
            }

            String currentVersionModFileName = modMeta.getVersionMap().get(version);

            File destinationModFile = new File(destinationModsDirectory, currentVersionModFileName);
            if (!destinationModFile.exists()) {
                if (result == MOD_VALIDATION.NO_UPDATE) {
                    result = MOD_VALIDATION.UPDATE;
                }

                Map<String, String> versionMap = modMeta.getVersionMap();
                Map<String, String> versionReverseMap = new HashMap<String, String>();
                for (String mcVersion : versionMap.keySet()) {
                    String file = versionMap.get(mcVersion);
                    versionReverseMap.put(file, mcVersion);
                }

                String currentVersion = null;
                for (File modFile : modFolderContents) {
                    String modFileNameString = modFile.getName();
                    if (versionReverseMap.containsKey(modFileNameString)) {
                        currentVersion = versionReverseMap.get(modFileNameString);

                        // Add current version to delete list.
                        deleteList.add(modFile);
                    }
                }

                if (currentVersion == null) {
                    System.out.println("New file required for " + modMeta.getName() + ": " + version);
                } else {
                    System.out.println("Update required for " + modMeta.getName() + ": " + currentVersion + " --> " + version);
                }

                File sourceModDirectory = new File(sourceModsBaseDirectory, modMeta.getName());
                File sourceModFile = new File(sourceModDirectory, currentVersionModFileName);

                if (!sourceModDirectory.exists() || !sourceModFile.exists()) {
                    throw new UpdateException("Configuration Error: " + modMeta.getName() + " has a config error with version \"" + version + "\" --> " + sourceModFile.getAbsolutePath());
                }

                copyList.add(sourceModFile);
            }
        }
        return result;
    }

    @Override
    protected void preUpdate() throws UpdateException {
        System.out.println("Mod Updater Detected " + deleteList.size() + " File(s) To Clean.");

        for (File deleteFile : deleteList) {
            System.out.println("Deleting: " + deleteFile.getAbsolutePath());
            deleteFile.delete();
        }
    }

    @Override
    protected void handleUpdate() throws UpdateException {
        for (File copyFile : copyList) {
            try {
                File destinationFile = new File(destinationModsDirectory, copyFile.getName());
                FileUtils.copyFile(copyFile, destinationFile);

                System.out.println("Updated " + copyFile.getAbsolutePath() + " --> " + destinationFile.getAbsolutePath());
            } catch (IOException ex) {
                Logger.getLogger(StructuredDirectoryModsUpdater.class.getName()).log(Level.SEVERE, null, ex);
                throw new UpdateException(ex);
            }
        }

    }

    @Override
    protected void postUpdate() throws UpdateException {

        System.out.println("Mod Updater has Updated " + copyList.size() + " Mods.");
    }

    public static class ModMeta {

        private String name;
        private boolean clientSide;
        private boolean serverSide;
        private Map<String, String> versionMap;
        private Map<String, String[]> modVersionMap;

        public ModMeta() {
            this.name = null;
            this.clientSide = false;
            this.serverSide = false;
            this.versionMap = null;
            this.modVersionMap = null;
        }

        public ModMeta(String name, boolean clientSide, boolean serverSide, Map<String, String> versionMap, Map<String, String[]> modVersionMap) {
            this.name = name;
            this.clientSide = clientSide;
            this.serverSide = serverSide;
            this.versionMap = versionMap;
            this.modVersionMap = modVersionMap;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isClientSide() {
            return clientSide;
        }

        public void setClientSide(boolean clientSide) {
            this.clientSide = clientSide;
        }

        public boolean isServerSide() {
            return serverSide;
        }

        public void setServerSide(boolean serverSide) {
            this.serverSide = serverSide;
        }

        public Map<String, String> getVersionMap() {
            return versionMap;
        }

        public void setVersionMap(Map<String, String> versionMap) {
            this.versionMap = versionMap;
        }

        public Map<String, String[]> getModVersionMap() {
            return modVersionMap;
        }

        public void setModVersionMap(Map<String, String[]> modVersionMap) {
            this.modVersionMap = modVersionMap;
        }
    }
}
