/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.slavitica.slaviticacraft.update;

import java.io.File;
import java.net.URL;

/**
 *
 * @author John
 */
public class DownloadMeta {

    private URL url;
    private File destination;

    public DownloadMeta() {
        url = null;
        destination = null;
    }

    public DownloadMeta(URL url, File destination) {
        this.url = url;
        this.destination = destination;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public File getDestination() {
        return destination;
    }

    public void setDestination(File destination) {
        this.destination = destination;
    }
    
    
}
