package net.slavitica.slaviticacraft.update;

public abstract class Download {

    public static enum DOWNLOAD_STATUS {

        NEW,
        SETUP,
        DOWNLOADING,
        DOWNLOADED,
        VERIFIED,
        RETRY,
        CLEANED
    }
    private DOWNLOAD_STATUS status;

    public void setup() throws DownloadException {
        if (status != DOWNLOAD_STATUS.NEW || status != DOWNLOAD_STATUS.CLEANED) {
            throw new DownloadException("Status not NEW");
        }
        handleSetup();
        status = DOWNLOAD_STATUS.SETUP;
    }

    public void download() throws DownloadException {
        if (status != DOWNLOAD_STATUS.SETUP) {
            throw new DownloadException("Status not SETUP");
        }
        status = DOWNLOAD_STATUS.DOWNLOADING;
        handleDownload();
        status = DOWNLOAD_STATUS.DOWNLOADED;
    }

    public void verify() throws DownloadException {
        if (status != DOWNLOAD_STATUS.DOWNLOADED) {
            throw new DownloadException("Status not DOWNLOADED");
        }
        handleVerify();
        status = DOWNLOAD_STATUS.VERIFIED;
    }

    public void retry() throws DownloadException {
        if (status != DOWNLOAD_STATUS.DOWNLOADING || status != DOWNLOAD_STATUS.DOWNLOADED) {
            throw new DownloadException("Status not DOWNLOADED");
        }
        status = DOWNLOAD_STATUS.RETRY;
        handleRetry();
    }

    public void clean() throws DownloadException {
        handleClean();
        status = DOWNLOAD_STATUS.CLEANED;
    }

    public abstract void handleSetup() throws DownloadException;

    public abstract void handleDownload() throws DownloadException;

    public abstract void handleVerify() throws DownloadException;

    public abstract void handleRetry() throws DownloadException;

    public abstract void handleClean() throws DownloadException;
}
