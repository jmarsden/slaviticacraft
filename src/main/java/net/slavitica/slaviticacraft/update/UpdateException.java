/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.slavitica.slaviticacraft.update;

/**
 *
 * @author John
 */
public class UpdateException extends Exception {

    public UpdateException() {
    }

    public UpdateException(String message) {
        super(message);
    }

    public UpdateException(String message, Throwable cause) {
        super(message, cause);
    }

    public UpdateException(Throwable cause) {
        super(cause);
    }

    public UpdateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
}
