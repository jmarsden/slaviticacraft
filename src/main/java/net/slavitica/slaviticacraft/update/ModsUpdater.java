/**
 * Copyright (C) 2014 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package net.slavitica.slaviticacraft.update;

public abstract class ModsUpdater {

    public enum UPDATE_MODE {

        CLIENT_AND_SERVER,
        CLIENT,
        SERVER
    }

    public enum MOD_VALIDATION {

        UPDATE,
        NO_UPDATE
    }

    public enum MOD_UPDATER_STATUS {

        NEW,
        INITALIZED,
        VALIDATED,
        UPDATED
    }
    protected UPDATE_MODE mode = UPDATE_MODE.CLIENT_AND_SERVER;
    private MOD_UPDATER_STATUS status;

    public ModsUpdater() {
        mode = UPDATE_MODE.CLIENT_AND_SERVER;
        status = MOD_UPDATER_STATUS.NEW;
    }

    public final void init() throws UpdateException {
        synchronized (this) {
            if (status != MOD_UPDATER_STATUS.NEW) {
                throw new RuntimeException("Mod Updater State Exception: Cannot init() from " + status);
            }
            handleInit();
            status = MOD_UPDATER_STATUS.INITALIZED;
        }
    }

    public final MOD_VALIDATION validate() throws UpdateException {
        synchronized (this) {
            if (status == MOD_UPDATER_STATUS.NEW) {
                init();
            } else if (status != MOD_UPDATER_STATUS.INITALIZED) {
                throw new RuntimeException("Mod Updater State Exception: Cannot validate() from " + status);
            }
            MOD_VALIDATION update = handleValidate();
            status = MOD_UPDATER_STATUS.VALIDATED;
            return update;
        }
    }

    public final void update() throws UpdateException {
        synchronized (this) {
            MOD_VALIDATION update = null;
            if (status == MOD_UPDATER_STATUS.NEW) {
                init();
                update = validate();
            } else if (status == MOD_UPDATER_STATUS.INITALIZED) {
                update = validate();
            } else if (status != MOD_UPDATER_STATUS.VALIDATED) {
                throw new RuntimeException("Mod Updater State Exception: Cannot update() from " + status);
            }
            if (update == null || update == MOD_VALIDATION.UPDATE) {
                preUpdate();
                handleUpdate();
                postUpdate();
            }
            status = MOD_UPDATER_STATUS.UPDATED;
        }
    }

    protected abstract void handleInit() throws UpdateException;

    protected abstract MOD_VALIDATION handleValidate() throws UpdateException;

    protected abstract void preUpdate() throws UpdateException;

    protected abstract void handleUpdate() throws UpdateException;

    protected abstract void postUpdate() throws UpdateException;

    @Override
    public String toString() {
        return "ModsUpdater[" + status + "]";
    }
}
