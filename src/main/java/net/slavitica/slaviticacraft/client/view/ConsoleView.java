/**
 * Copyright (C) 2014 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package net.slavitica.slaviticacraft.client.view;

import java.awt.Color;
import java.io.IOException;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import net.slavitica.slaviticacraft.Controller;
import net.slavitica.slaviticacraft.View;
import net.slavitica.slaviticacraft.client.Instance;
import net.slavitica.slaviticacraft.client.InstanceListener;
import net.slavitica.slaviticacraft.client.console.ConsoleMessage;
import net.slavitica.slaviticacraft.client.controller.ConsoleController;
import net.slavitica.slaviticacraft.client.controller.intent.LoginLogedInIntent;
import net.slavitica.slaviticacraft.client.controller.intent.RootInstanceCreatedIntent;
import net.slavitica.slaviticacraft.client.controller.intent.RootSuppressStandardOutIntent;
import net.slavitica.slaviticacraft.client.controller.intent.RootWindowClosingIntent;
import net.slavitica.slaviticacraft.messaging.Intent;
import net.slavitica.slaviticacraft.messaging.IntentHub;
import net.slavitica.slaviticacraft.messaging.IntentReceiver;

public class ConsoleView extends JScrollPane implements InstanceListener, View, IntentReceiver {

    private JEditorPane editorPane;
    private HTMLEditorKit kit;
    private HTMLDocument doc;
    private ConsoleController controller;

    public ConsoleView(ConsoleController controller) {
        this.controller = controller;
    }

    @Override
    public void addNotify() {
        super.addNotify();

        editorPane = new JEditorPane("text/html", "") {
            @Override
            public boolean getScrollableTracksViewportWidth() {
                return true;
            }
        };
        kit = new HTMLEditorKit();

        StyleSheet styleSheet = kit.getStyleSheet();
        
        styleSheet.addRule("body { background-color: #FFFFFF; color: red; }");
        styleSheet.addRule(".d {color: #0000FF; font-family: monospace; }");
        styleSheet.addRule(".mon {color: #0000FF; font-family: monospace; }");
        
        styleSheet.addRule(".h {color: #00FFFF; font-family: monospace; }");
        styleSheet.addRule(".min {color: #00FFFF; font-family: monospace; }");
        styleSheet.addRule(".s {color: #00FFFF; font-family: monospace; }");
        
        styleSheet.addRule(".sep {color: #AAAAAA; font-family: monospace; }");
        styleSheet.addRule(".info {color: #0000FF; font-family: monospace; }");
        styleSheet.addRule(".warn {color: #FFFF00; font-family: monospace; }");
        styleSheet.addRule(".error {color: #FF0000; font-family: monospace; }");
        
        styleSheet.addRule(".mod {color: #00FF00; font-family: monospace; }");
        
        doc = (HTMLDocument) kit.createDefaultDocument();
        editorPane.setEditable(false);
        editorPane.setSelectionColor(Color.GRAY);
        editorPane.setEditorKit(kit);
        editorPane.setDocument(doc);




        setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_AS_NEEDED);
        setVerticalScrollBarPolicy(VERTICAL_SCROLLBAR_AS_NEEDED);

        getViewport().add(editorPane);

        IntentHub.lookupSingleton().registerForIntent(this, LoginLogedInIntent.class);
        IntentHub.lookupSingleton().registerForIntent(this, RootInstanceCreatedIntent.class);
        IntentHub.lookupSingleton().registerForIntent(this, RootWindowClosingIntent.class);
    }

    public Controller getController() {
        return controller;
    }

    public void append(String text) {
        try {
            doc.insertString(doc.getLength(), text + "\n", null);
            editorPane.setCaretPosition(doc.getLength());
        } catch (BadLocationException ex) {
            Logger.getLogger(ConsoleView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void append(ConsoleMessage message) {
        try {
            StringReader reader = new StringReader(message.toConsoleOutput() + "<br />");
            kit.read(reader, doc, doc.getLength());
            editorPane.setCaretPosition(doc.getLength());
        } catch (IOException ex) {
            Logger.getLogger(ConsoleView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadLocationException ex) {
            Logger.getLogger(ConsoleView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Notification Method For Instance notification.
     *
     * @param content
     */
    @Override
    public void notify(String content) {
        ConsoleMessage message = controller.addConsoleMessage(content);
        if (message != null) {
            append(message);
        } else {
            System.out.println(content);
            append(content);
        }
    }

    public void exit(String content) {
    }

    public void receiveIntent(Intent intent) {
        if (intent instanceof LoginLogedInIntent) {
            notify("Login!");
        } else if (intent instanceof RootInstanceCreatedIntent) {
            RootInstanceCreatedIntent rootIntanceCreatedIntent = (RootInstanceCreatedIntent) intent;
            Instance instance = rootIntanceCreatedIntent.getInstance();
            instance.addListener(this);
            IntentHub.lookupSingleton().notifyIntent(new RootSuppressStandardOutIntent(controller, true));
        } else if (intent instanceof RootWindowClosingIntent) {
            IntentHub.lookupSingleton().notifyIntent(new RootSuppressStandardOutIntent(controller, false));
        }
    }
}
