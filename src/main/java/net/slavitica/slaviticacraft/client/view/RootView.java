/**
 * Copyright (C) 2014 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package net.slavitica.slaviticacraft.client.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import net.slavitica.slaviticacraft.Controller;
import net.slavitica.slaviticacraft.View;
import net.slavitica.slaviticacraft.client.Token;
import net.slavitica.slaviticacraft.client.controller.intent.LoginLogedInIntent;
import net.slavitica.slaviticacraft.client.controller.RootController;
import net.slavitica.slaviticacraft.client.controller.intent.ClientWindowRelaunchedIntent;
import net.slavitica.slaviticacraft.client.controller.intent.LoginLogOutIntent;
import net.slavitica.slaviticacraft.client.controller.intent.RootStatusMessageIntent;
import net.slavitica.slaviticacraft.client.controller.intent.RootSuppressStandardOutIntent;
import net.slavitica.slaviticacraft.messaging.Intent;
import net.slavitica.slaviticacraft.messaging.IntentHub;
import net.slavitica.slaviticacraft.messaging.IntentReceiver;

public class RootView extends JFrame implements View, IntentReceiver {

    RootController controller;
    JLabel statusLabel;
    JTabbedPane tabbedPane;

    public RootView(RootController controller) {
        this.controller = controller;
        tabbedPane = new JTabbedPane();
    }

    public RootView(RootController controller, GraphicsConfiguration gc) {
        super(gc);
        this.controller = controller;
        tabbedPane = new JTabbedPane();
    }

    public RootView(RootController controller, String title) throws HeadlessException {
        super(title);
        this.controller = controller;
        tabbedPane = new JTabbedPane();
    }

    public RootView(RootController controller, String title, GraphicsConfiguration gc) {
        super(title, gc);
        this.controller = controller;
        tabbedPane = new JTabbedPane();
    }

    @Override
    public void addNotify() {
        super.addNotify();
        tabbedPane.setTabPlacement(JTabbedPane.BOTTOM);
        getContentPane().add(tabbedPane, BorderLayout.CENTER);

        JPanel statusPanel = new JPanel();
        add(statusPanel, BorderLayout.SOUTH);
        statusPanel.setPreferredSize(new Dimension(getWidth(), 16));
        statusPanel.setLayout(new BoxLayout(statusPanel, BoxLayout.X_AXIS));
        statusLabel = new JLabel("");
        statusLabel.setHorizontalAlignment(SwingConstants.LEFT);
        statusPanel.add(statusLabel);

        IntentHub.lookupSingleton().registerForIntent(this, RootStatusMessageIntent.class);
        IntentHub.lookupSingleton().registerForIntent(this, RootSuppressStandardOutIntent.class);
        IntentHub.lookupSingleton().registerForIntent(this, LoginLogedInIntent.class);
        IntentHub.lookupSingleton().registerForIntent(this, LoginLogOutIntent.class);
        IntentHub.lookupSingleton().registerForIntent(this, ClientWindowRelaunchedIntent.class);
    }

    public Controller getController() {
        return controller;
    }

    public void receiveIntent(Intent intent) {
        if (intent instanceof RootStatusMessageIntent) {
            RootStatusMessageIntent statusMessageIntent = (RootStatusMessageIntent) intent;
            statusLabel.setText(" " + statusMessageIntent.getMessage());
        } else if (intent instanceof LoginLogedInIntent) {
            LoginLogedInIntent loginIntent = (LoginLogedInIntent) intent;
            Token token = loginIntent.getToken();
            
            controller.runClient(token);
            if (hasTab("Console")) {
                selectTab("Console");
            }
        } else if (intent instanceof LoginLogOutIntent) {
            controller.killInstance();
        } else if(intent instanceof ClientWindowRelaunchedIntent) {
            controller.runClient();
            if (hasTab("Console")) {
                selectTab("Console");
            }
        } else if (intent instanceof RootSuppressStandardOutIntent) {
            RootSuppressStandardOutIntent suppressStandardOutIntent = (RootSuppressStandardOutIntent) intent;
            controller.handleStandardOutputSuppression(suppressStandardOutIntent.isStatus());
        }
    }

    public void handleClose() {
    }

    public boolean hasTab(String name) {
        int size = tabbedPane.getTabCount();
        for (int i = 0; i < size; i++) {
            String tabName = tabbedPane.getTitleAt(i);
            if (tabName.equals(name)) {
                return true;
            }
        }
        return false;
    }

    public void addTab(String name, Component component) {
        if (hasTab(name)) {
            return;
        }
        tabbedPane.add(name, component);
    }

    public void removeTab(String name) {
        int size = tabbedPane.getTabCount();
        for (int i = 0; i < size; i++) {
            String tabName = tabbedPane.getTitleAt(i);
            if (tabName.equals(name)) {
                tabbedPane.removeTabAt(i);
                break;
            }
        }
    }

    public void selectTab(String name) {
        int size = tabbedPane.getTabCount();
        for (int i = 0; i < size; i++) {
            String tabName = tabbedPane.getTitleAt(i);
            if (tabName.equals(name)) {
                tabbedPane.setSelectedIndex(i);
                break;
            }
        }
    }

    public static class Tab {

        String tabName;
        Component component;

        public Tab(String tabName, Component component) {
            this.tabName = tabName;
            this.component = component;
        }

        public String getTabName() {
            return tabName;
        }

        public void setTabName(String tabName) {
            this.tabName = tabName;
        }

        public Component getComponent() {
            return component;
        }

        public void setComponent(Component component) {
            this.component = component;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 73 * hash + (this.tabName != null ? this.tabName.hashCode() : 0);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Tab other = (Tab) obj;
            if ((this.tabName == null) ? (other.tabName != null) : !this.tabName.equals(other.tabName)) {
                return false;
            }
            return true;
        }
    }
}
