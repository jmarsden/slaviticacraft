/**
 * Copyright (C) 2014 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package net.slavitica.slaviticacraft.client.view;

import cc.plural.jsonij.parser.ParserException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import net.slavitica.slaviticacraft.App;
import net.slavitica.slaviticacraft.View;
import net.slavitica.slaviticacraft.client.Token;
import net.slavitica.slaviticacraft.client.Version;
import net.slavitica.slaviticacraft.client.controller.LoginController;
import net.slavitica.slaviticacraft.client.controller.intent.ClientWindowClosedIntent;
import net.slavitica.slaviticacraft.client.controller.intent.ClientWindowRelaunchedIntent;
import net.slavitica.slaviticacraft.client.controller.intent.LoginLogOutIntent;
import net.slavitica.slaviticacraft.client.controller.intent.LoginLogedInIntent;
import net.slavitica.slaviticacraft.client.controller.intent.LoginLoginFailIntent;
import net.slavitica.slaviticacraft.messaging.Intent;
import net.slavitica.slaviticacraft.messaging.IntentHub;
import net.slavitica.slaviticacraft.messaging.IntentReceiver;

public class LoginView extends JPanel implements View, IntentReceiver {

    final LoginController controller;
    final JTextField usernameField;
    final JPasswordField passwordField;
    final JButton loginButton;
    final JButton launchButton;
    JButton updateButton;
    JComboBox versionList;
    JComboBox updaterList;
    Image background;
    int backgroundHeight;
    int backgroundWidth;
    double halfBackgroundHeight;
    double halfBackgroundWidth;
    final ActionListener enterAction;
    final ActionListener launchAction;
    final ActionListener updateAction;

    public LoginView(LoginController loginController) {
        this.controller = loginController;

        usernameField = new JTextField("", 20);
        passwordField = new JPasswordField("", 20);
        loginButton = new JButton("Login");
        loginButton.setPreferredSize(new Dimension(90, 35));

        launchButton = new JButton("Relaunch");
        launchButton.setPreferredSize(new Dimension(90, 35));
        launchButton.setEnabled(false);

        enterAction = new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                if (controller.isLoggedIn()) {
                    controller.logOut(null);
                } else {

                    int updaterIndex = updaterList.getSelectedIndex();
                    String updater = updaterList.getSelectedItem().toString();
                    try {
                        Class<?> updaterClass = Class.forName(updater);
                        controller.setUpdater(updaterClass);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(LoginView.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    int versionIndex = versionList.getSelectedIndex();
                    controller.setSelectedVersion(versionIndex);
                    
                    String username = usernameField.getText();
                    String password = passwordField.getText();
                    //todo: Error Check
                    Token token = controller.login(username, password);
                    
                    controller.runInstance(token);
                }
            }
        };

        launchAction = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                controller.relaunch();
            }
        };
        
        updateAction = new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                Version version = (Version) versionList.getSelectedItem();
                try {
                    controller.update(version);
                } catch (MalformedURLException ex) {
                    Logger.getLogger(LoginView.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(LoginView.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ParserException ex) {
                    Logger.getLogger(LoginView.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };

        if (!App.DEBUG) {
            URL imageURL = this.getClass().getResource("/background.png");
            if (imageURL != null) {
                ImageIcon imageIcon = new ImageIcon(imageURL);
                if (imageIcon != null) {
                    background = imageIcon.getImage();
                    backgroundHeight = background.getHeight(null);
                    backgroundWidth = background.getWidth(null);
                    halfBackgroundHeight = backgroundHeight / 2D;
                    halfBackgroundWidth = backgroundWidth / 2D;
                }
            }
        }
    }

    @Override
    public void addNotify() {
        super.addNotify();

        JPanel usernamePanel = new JPanel();
        usernamePanel.setOpaque(false);
        usernamePanel.add(usernameField);

        JPanel passwordPanel = new JPanel();
        passwordPanel.setOpaque(false);
        passwordPanel.add(passwordField);

        JPanel inputFieldPanel = new JPanel(new BorderLayout());
        inputFieldPanel.setOpaque(false);
        inputFieldPanel.add(usernamePanel, BorderLayout.NORTH);
        inputFieldPanel.add(passwordPanel, BorderLayout.SOUTH);

        JPanel buttonGroupPanel = new JPanel(new BorderLayout());
        buttonGroupPanel.setOpaque(false);
        buttonGroupPanel.add(loginButton, BorderLayout.NORTH);
        buttonGroupPanel.add(launchButton, BorderLayout.SOUTH);

        JPanel group = new JPanel(new FlowLayout());
        group.setOpaque(false);
        group.add(inputFieldPanel);
        group.add(buttonGroupPanel);

        JPanel loginPanel = new JPanel(new BorderLayout());
        loginPanel.setOpaque(false);
        loginPanel.add(group, BorderLayout.NORTH);

        updateButton = new JButton("Update");
        updateButton.addActionListener(updateAction);
        
        JPanel optionPanel = new JPanel(new FlowLayout());
        optionPanel.setBackground(Color.BLACK);
        Version[] versions = controller.getVersions();
        JLabel versionLabel = new JLabel("Version");
        versionLabel.setForeground(Color.WHITE);
        versionList = new JComboBox(versions);

        JLabel updaterLabel = new JLabel("Mods Updater");
        updaterLabel.setForeground(Color.WHITE);
        updaterList = new JComboBox(new String[]{"net.slavitica.slaviticacraft.update.StructuredDirectoryModsUpdater"});

        
        optionPanel.add(versionLabel);
        optionPanel.add(versionList);
        optionPanel.add(updateButton);
        //optionPanel.add(updaterLabel);
        //optionPanel.add(updaterList);

        setLayout(new BorderLayout());
        setBackground(Color.BLACK);
        add(loginPanel, BorderLayout.NORTH);
        add(optionPanel, BorderLayout.SOUTH);

        loginButton.addActionListener(enterAction);
        launchButton.addActionListener(launchAction);

        usernameField.addActionListener(enterAction);
        passwordField.addActionListener(enterAction);

        IntentHub.lookupSingleton().registerForIntent(this, LoginLogedInIntent.class);
        IntentHub.lookupSingleton().registerForIntent(this, LoginLogOutIntent.class);
        IntentHub.lookupSingleton().registerForIntent(this, LoginLoginFailIntent.class);
        IntentHub.lookupSingleton().registerForIntent(this, ClientWindowClosedIntent.class);
        IntentHub.lookupSingleton().registerForIntent(this, ClientWindowRelaunchedIntent.class);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (background != null) {
            int height = getHeight();
            int width = getWidth();

            double halfHeight = height / 2D;
            double halfWidth = width / 2D;

            int startWidth = (int) (halfWidth - halfBackgroundWidth);
            int startHeight = (int) (halfHeight - halfBackgroundHeight);

            g.drawImage(background, startWidth, startHeight, backgroundWidth, backgroundHeight, this);
        }
    }

    @Override
    public LoginController getController() {
        return controller;
    }

    public void handleLogin() {
        usernameField.setEnabled(false);
        passwordField.setText("");
        passwordField.setEnabled(false);
        versionList.setEnabled(false);
        updaterList.setEnabled(false);
        updateButton.setEnabled(false);
        loginButton.setText("Logout");
    }

    public void handleLogout() {
        usernameField.setEnabled(true);
        passwordField.setEnabled(true);
        launchButton.setEnabled(false);
        versionList.setEnabled(true);
        updaterList.setEnabled(true);
        updateButton.setEnabled(true);
        loginButton.setText("Login");
    }

    public void handleClientClosed() {
        launchButton.setEnabled(true);
    }

    public void handleRelaunch() {
        launchButton.setEnabled(false);
    }

    public void handleFail() {
        // Error Message
    }

    public void receiveIntent(Intent intent) {
        if (intent instanceof LoginLoginFailIntent) {
            handleFail();
        } else if (intent instanceof LoginLogedInIntent) {
            handleLogin();
        } else if (intent instanceof LoginLogOutIntent) {
            handleLogout();
        } else if (intent instanceof ClientWindowClosedIntent) {
            handleClientClosed();
        } else if (intent instanceof ClientWindowRelaunchedIntent) {
            handleRelaunch();
        }
    }
}
