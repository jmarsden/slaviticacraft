/**
 * Copyright (C) 2014 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package net.slavitica.slaviticacraft.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Instance implements Runnable {

    private boolean running;
    ClientConfiguration clientConfiguration;
    List<InstanceListener> instanceListeners;
    private BufferedWriter bufferedWriter;
    private BufferedReader bufferedReader;
    Process process = null;

    public Instance(ClientConfiguration clientConfiguration) {
        this.clientConfiguration = clientConfiguration;
        this.instanceListeners = new ArrayList<InstanceListener>();

        running = false;
    }

    public boolean isRunning() {
        return running;
    }

    public void stopInstance() {
        synchronized (this) {
            this.running = false;
            try {
                bufferedReader.close();
                for (InstanceListener instanceListener : instanceListeners) {
                    instanceListener.notify("Instance Close Signal.");
                }
            } catch (IOException ex) {
                for (InstanceListener instanceListener : instanceListeners) {
                    instanceListener.notify("Instance Close Signal Error: " + ex.toString() + ".");
                }
            }
        }
    }
    
    public void destroyInstance() {
        synchronized (this) {
            process.destroy();
        }
    }

    public void addListener(InstanceListener listener) {
        instanceListeners.add(listener);
    }

    public void removeListener(InstanceListener listener) {
        instanceListeners.remove(listener);
    }

    public void run() {
        synchronized (this) {
            if (running) {
                return;
            }
        }
        List<String> arguments = clientConfiguration.getProcessBuilderArguments();
        ProcessBuilder processBuilder = new ProcessBuilder(arguments);
        processBuilder.redirectErrorStream(true);

        try {
            process = processBuilder.start();
            InputStream inputStream = process.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            bufferedReader = new BufferedReader(inputStreamReader);

            OutputStream outputStream = process.getOutputStream();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
            bufferedWriter = new BufferedWriter(outputStreamWriter);

            String line;
            synchronized (this) {
                running = true;
            }

            while (running && (line = bufferedReader.readLine()) != null) {
                if (instanceListeners != null) {
                    for (InstanceListener instanceListener : instanceListeners) {
                        instanceListener.notify(line);
                    }
                }
            }

            
            if (!running) {
                for (InstanceListener instanceListener : instanceListeners) {
                    instanceListener.notify("Instance Exit.");
                }
            } else {
                running = false;
                for (InstanceListener instanceListener : instanceListeners) {
                    instanceListener.exit("Client Closed.");
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Instance.class.getName()).log(Level.SEVERE, null, ex);
            if (instanceListeners != null) {
                for (InstanceListener instanceListener : instanceListeners) {
                    instanceListener.notify(ex.toString());
                }
            }
        } finally {
            if (process != null) {
                process.destroy();
            }
        }
    }
}
