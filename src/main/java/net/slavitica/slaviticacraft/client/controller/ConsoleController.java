/**
 * Copyright (C) 2014 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package net.slavitica.slaviticacraft.client.controller;

import net.slavitica.slaviticacraft.Controller;
import net.slavitica.slaviticacraft.Model;
import net.slavitica.slaviticacraft.client.console.ConsoleMessage;
import net.slavitica.slaviticacraft.client.console.ConsoleMessageParser;
import net.slavitica.slaviticacraft.client.model.ClientModel;

/**
 *
 * @author John
 */
public class ConsoleController extends Controller {

    public ConsoleController(Model model) {
        super(model);
    }
    
    public ConsoleMessage addConsoleMessage(String message) {
        ConsoleMessage consoleMessage = ConsoleMessageParser.parseMessage(message);
        if(consoleMessage == null) {
            return null;
        }
        ClientModel clientModel = (ClientModel) getModel();
        
        
        //System.out.println(consoleMessage);
        
        return consoleMessage;
    }
}
