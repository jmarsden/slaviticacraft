/**
 * Copyright (C) 2014 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package net.slavitica.slaviticacraft.client.controller;

import net.slavitica.slaviticacraft.client.controller.intent.RootInstanceCreatedIntent;
import net.slavitica.slaviticacraft.client.controller.intent.RootStatusMessageIntent;
import net.slavitica.slaviticacraft.client.controller.intent.RootInstanceRunningIntent;
import net.slavitica.slaviticacraft.client.controller.intent.RootWindowClosingIntent;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.slavitica.slaviticacraft.Controller;
import net.slavitica.slaviticacraft.client.ClientConfiguration;
import net.slavitica.slaviticacraft.client.Instance;
import net.slavitica.slaviticacraft.client.InstanceListener;
import net.slavitica.slaviticacraft.client.Token;
import net.slavitica.slaviticacraft.client.Version;
import net.slavitica.slaviticacraft.client.controller.intent.ClientWindowClosedIntent;
import net.slavitica.slaviticacraft.client.model.ClientModel;
import net.slavitica.slaviticacraft.messaging.IntentHub;
import net.slavitica.slaviticacraft.update.ModsUpdater;
import net.slavitica.slaviticacraft.update.StructuredDirectoryModsUpdater;
import net.slavitica.slaviticacraft.update.UpdateException;

public class RootController extends Controller {

    private static final Logger LOGGER = Logger.getLogger(RootController.class.getSimpleName());
    private Instance instance;
    public final StandardOut standardOut;

    static {
        //STANDARD_OUT = new StandardOut();
    }

    public RootController(ClientModel model) {
        super(model);
        instance = null;
        standardOut = new StandardOut();
    }

    public void handleClose() {
        IntentHub.lookupSingleton().notifyIntent(new RootWindowClosingIntent(this));
        if (instance != null) {
            instance.stopInstance();
        }
    }

    public void statusMessage(String message) {
        IntentHub.lookupSingleton().notifyIntent(new RootStatusMessageIntent(this, new Date(), message));
    }

    public void statusMessage(Date date, String message) {
        IntentHub.lookupSingleton().notifyIntent(new RootStatusMessageIntent(this, date, message));
    }

    public void runClient(Token token) {
        ClientModel model = (ClientModel) getModel();
        model.setToken(token);

        statusMessage("Instance Init.");
        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "Instance Init.");
        }

        init();

        statusMessage("Update Check.");
        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "Update Check.");
        }
        updateCheck();

        statusMessage("Running Client Instance.");
        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "Running Client Instance.");
        }
        runClient();
    }

    public void init() {
        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "Initalize Root Controller.");
        }
        try {
            ClientModel model = (ClientModel) getModel();
            File base = model.getBaseDirectory();

            Version target = null;
            String targetVersion = "1.6.4";
            Version[] versions = model.getVersions();
            if (versions != null) {
                target = versions[model.getSelectedVersion()];
                targetVersion = target.getVersion();
            }

            if (LOGGER.isLoggable(Level.INFO)) {
                LOGGER.log(Level.INFO, "Target Version: {0}", targetVersion);
            }

            if (LOGGER.isLoggable(Level.INFO)) {
                LOGGER.log(Level.INFO, "Base: {0}", base.getAbsolutePath());
            }
            if (base.exists()) {
                model.setBaseDirectory(base);

                File resourcesDirectory = new File(base, "resources");
                if (!resourcesDirectory.exists()) {
                    resourcesDirectory.mkdirs();
                }
                File resourcesFolder = new File(resourcesDirectory, target.getResources());
                if (!resourcesFolder.exists()) {
                    resourcesFolder.mkdirs();
                }
                
                model.setResourcesDirectory(resourcesFolder);
                File deploymentsDirectory = new File(base, "deployments");
                if (!deploymentsDirectory.exists()) {
                    deploymentsDirectory.mkdirs();
                }

                File deploymentFolder = null;
                File[] deploymentDirectoryList = deploymentsDirectory.listFiles();
                int i = 0;
                for (File version : deploymentDirectoryList) {
                    if (version.isDirectory()) {
                        String folderName = version.getName();
                        if (folderName.equals(targetVersion)) {
                            deploymentFolder = version;
                            break;
                        }
                    }
                    i++;
                }
                if (deploymentFolder == null) {
                    LOGGER.log(Level.SEVERE, "No Version Deployment Folder");
                    throw new RuntimeException();
                }
                model.setDeploymentDirectory(deploymentFolder);
                model.setSelectedVersion(i);

                File binDirectory = new File(deploymentFolder, "bin");
                File nativeDirectory = new File(binDirectory, "natives");
                File jarMods = new File(deploymentFolder, "jarmods");
                if (!jarMods.exists()) {
                    jarMods.mkdirs();
                    // Download Forge Craft
                }
                File modDirectory = new File(deploymentFolder, "mods");
                File configDirectory = new File(deploymentFolder, "config");

                model.setBinDirectory(binDirectory);
                model.setNativeDirectory(nativeDirectory);
                model.setJarMods(jarMods);
                model.setModDirectory(modDirectory);
                model.setConfigDirectory(configDirectory);
            } else {
                LOGGER.log(Level.SEVERE, "Base not found: {0}", base);
                statusMessage("Could not find base directory. Move jar to right location.");
            }

        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Generic Init Exception", e);
        }
    }

    public void updateCheck() {
        ClientModel model = (ClientModel) getModel();

        String userHome = System.getProperty("user.home");
        if (userHome == null) {
            throw new RuntimeException("No user.home??");
        }

        File userHomeDirectory = new File(userHome);
        File dropbox = new File(userHomeDirectory, "Dropbox");
        File mcModCache = new File(dropbox, "MCModCache");


        ModsUpdater updater = new StructuredDirectoryModsUpdater("1.6.4", mcModCache, model.getModDirectory());
        try {
            if (LOGGER.isLoggable(Level.INFO)) {
                LOGGER.log(Level.INFO, "Checking Update.");
            }
            updater.init();
            ModsUpdater.MOD_VALIDATION update = updater.validate();
            if (update == ModsUpdater.MOD_VALIDATION.UPDATE) {
                if (LOGGER.isLoggable(Level.INFO)) {
                    LOGGER.log(Level.INFO, "Update Required.");
                }
                updater.update();
            } else {
                if (LOGGER.isLoggable(Level.INFO)) {
                    LOGGER.log(Level.INFO, "No Update Required.");
                }
            }
        } catch (UpdateException ex) {
            LOGGER.log(Level.SEVERE, "Exception Updating Mods", ex);
        }

    }

    public void runClient() {
        ClientModel model = (ClientModel) getModel();

        List<File> jarList = new ArrayList<File>();
        if (model.getBinDirectory().exists()) {
            File[] files = model.getBinDirectory().listFiles();
            for (File file : files) {
                if (file.isDirectory()) {
                    continue;
                }
                jarList.add(file);
            }
        }
        if (model.getJarMods().exists()) {
            File[] files = model.getJarMods().listFiles();
            for (File file : files) {
                if (file.isDirectory()) {
                    continue;
                }
                jarList.add(file);
            }
        }
        ClientConfiguration config = new ClientConfiguration();

        String javaHomePath = System.getenv("JAVA_HOME");
        if (javaHomePath == null) {
            javaHomePath = System.getProperty("java.home");
        }
        if (javaHomePath == null) {
            if (LOGGER.isLoggable(Level.SEVERE)) {
                LOGGER.log(Level.SEVERE, "Set a JAVA_HOME F000L!");
            }
            System.exit(0);
        }

        File javaHome = new File(javaHomePath);
        File javaBin = new File(javaHome, "bin");
        File javaBinary;

        String osName = System.getProperty("os.name");

        if (osName.startsWith("Windows")) {
            javaBinary = new File(javaBin, "javaw.exe");
        } else {
            javaBinary = new File(javaBin, "java");
        }

        config.setJavaExecutablePath(javaBinary.getAbsolutePath());
        config.setLibraryPath(model.getNativeDirectory());
        config.setClassPathList(jarList);
        config.setUsername(model.getToken().getProfileName());
        config.setToken(model.getToken());
        config.setMainClassName("net.minecraft.launchwrapper.Launch");
        config.setVersion("1.6.4");
        config.setGameDirectory(model.getDeploymentDirectory());
        config.setAssetDirectory(model.getResourcesDirectory());

        instance = new Instance(config);

        instance.addListener(standardOut);

        IntentHub.lookupSingleton().notifyIntent(new RootInstanceCreatedIntent(this, instance));
        Thread instanceThread = new Thread(instance);
        instanceThread.start();

        IntentHub.lookupSingleton().notifyIntent(new RootInstanceRunningIntent(this, instance));
        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "Process Command: {0}.", config.toString());
        }
    }

    public void handleStandardOutputSuppression(boolean status) {
        if (status) {
            standardOut.forceNotify("Suppressing Standard Output.");
        } else {
            standardOut.forceNotify("Activating Standard Output.");
        }
        standardOut.setOutputSuppressed(status);
    }

    public void handleInstanceExit() {
        IntentHub.lookupSingleton().notifyIntent(new ClientWindowClosedIntent(this));
    }

    public void killInstance() {
        if (instance != null) {
            instance.stopInstance();
            instance.destroyInstance();
        }
    }

    public class StandardOut implements InstanceListener {

        private boolean outputSuppressed;

        public StandardOut() {
            outputSuppressed = false;
        }

        public boolean isOutputSuppressed() {
            return outputSuppressed;
        }

        public void setOutputSuppressed(boolean outputSuppressed) {
            this.outputSuppressed = outputSuppressed;
        }

        @Override
        public void notify(String content) {
            if (!outputSuppressed) {
                System.out.println(content);
            }
        }

        public void forceNotify(String content) {
            System.out.println(content);
        }

        @Override
        public void exit(String content) {
            handleInstanceExit();

            System.out.println("Instance Stopped: " + content);
        }
    }
}
