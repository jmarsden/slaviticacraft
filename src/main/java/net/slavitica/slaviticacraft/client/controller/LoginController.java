/**
 * Copyright (C) 2014 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package net.slavitica.slaviticacraft.client.controller;

import cc.plural.jsonij.JPath;
import cc.plural.jsonij.JSON;
import cc.plural.jsonij.Value;
import cc.plural.jsonij.marshal.JSONMarshaler;
import cc.plural.jsonij.marshal.codec.DateJSONValueCodec;
import cc.plural.jsonij.parser.ParserException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.slavitica.slaviticacraft.App;
import net.slavitica.slaviticacraft.mojang.MCVersion;
import net.slavitica.slaviticacraft.client.AuthenticationException;
import net.slavitica.slaviticacraft.client.MinecraftAuthentication;
import net.slavitica.slaviticacraft.client.Token;
import net.slavitica.slaviticacraft.client.Version;
import net.slavitica.slaviticacraft.client.controller.intent.ClientWindowRelaunchedIntent;
import net.slavitica.slaviticacraft.client.controller.intent.LoginLogOutIntent;
import net.slavitica.slaviticacraft.client.controller.intent.LoginLogedInIntent;
import net.slavitica.slaviticacraft.client.controller.intent.LoginLoginFailIntent;
import net.slavitica.slaviticacraft.client.model.ClientModel;
import net.slavitica.slaviticacraft.messaging.IntentHub;
import net.slavitica.slaviticacraft.mojang.MCLatest;
import net.slavitica.slaviticacraft.mojang.MCLibrary;
import net.slavitica.slaviticacraft.mojang.MCResource;
import net.slavitica.slaviticacraft.mojang.MCResources;
import net.slavitica.slaviticacraft.update.DownloadMeta;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

public class LoginController extends RootController {

    private static final Logger LOGGER = Logger.getLogger(LoginController.class.getSimpleName());
    boolean loggedIn;

    public LoginController(ClientModel model) {
        super(model);
        loggedIn = false;
    }

//    public List<String> loadVersions() {
//        if (LOGGER.isLoggable(Level.INFO)) {
//            LOGGER.log(Level.INFO, "Loading Versions.");
//        }
//        List<String> versions = new ArrayList<String>();
//        ClientModel model = (ClientModel) getModel();
//        File base = model.getBaseDirectory();
//        File versionDirectory = new File(base, "versions");
//        if (!versionDirectory.exists()) {
//            throw new RuntimeException();
//        }
//        //File[] versionList = versionDirectory.listFiles();
//        model.setVersions(versionList);
//        int i = 0;
//        for (File version : versionList) {
//            if (version.isDirectory()) {
//                String folderName = version.getName();
//                versions.add(folderName);
//            }
//            i++;
//        }
//        if (LOGGER.isLoggable(Level.INFO)) {
//            LOGGER.log(Level.INFO, "Found {0} Versions.", i);
//        }
//        return versions;
//    }
    public void setUpdater(Class<?> updater) {
    }

    public void setSelectedVersion(int selectedVersion) {
        ClientModel model = (ClientModel) getModel();
        model.setSelectedVersion(selectedVersion);
    }

    public Token login(String username, String password) {
        if (App.DEBUG) {
            statusMessage("Using Offline Token.");
            if (LOGGER.isLoggable(Level.INFO)) {
                LOGGER.log(Level.INFO, "Using Offline Token.");
            }

            return Token.OFFLINE_TOKEN;
        }

        if (username == null || password == null || username.trim().equals("") || password.trim().equals("")) {
            statusMessage("Username or Password is Empty.");
            if (LOGGER.isLoggable(Level.INFO)) {
                LOGGER.log(Level.INFO, "Username or Password is Empty.");
            }
            return null;
        }
        Token token = null;
        try {
            token = MinecraftAuthentication.authenticateToken(username, password);
        } catch (AuthenticationException ex) {
            loginFail(username, ex);

        }
        return token;
    }

    public void loginFail(String username, AuthenticationException ex) {
        String message = "Failed to login: " + ex.toString();
        statusMessage(message);
        if (LOGGER.isLoggable(Level.SEVERE)) {
            LOGGER.log(Level.SEVERE, "Failed to login: {0}", ex.toString());
        }
        IntentHub.lookupSingleton().notifyIntent(new LoginLoginFailIntent(this, username, ex));
    }

    public void runInstance(Token token) {
        loggedIn = true;
        statusMessage("Successfully logged into account: " + token.getProfileName() + "");
        IntentHub.lookupSingleton().notifyIntent(new LoginLogedInIntent(this, token));
    }

    public void logOut(Token token) {
        loggedIn = false;
        statusMessage("Successfully logged out of account" + ((token != null) ? ": " + token.getProfileName() : ""));
        IntentHub.lookupSingleton().notifyIntent(new LoginLogOutIntent(this));
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void relaunch() {
        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "Relaunch Requested.");
        }
        IntentHub.lookupSingleton().notifyIntent(new ClientWindowRelaunchedIntent(this));
    }

    public Version[] getVersions() {
        ClientModel model = (ClientModel) getModel();
        return model.getVersions();
    }

    public void update(Version version) throws MalformedURLException, IOException, ParserException {
        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "Update Check for Version {0}.", version);
        }

        ClientModel model = (ClientModel) getModel();
        File base = model.getBaseDirectory();
        if (base == null) {
            base = new File(".");
        }
        File tmp = new File(base, "tmp");
        if (!tmp.exists()) {
            tmp.mkdirs();
        } else {
            
        }
        String versionName = version.getVersion();

        if (!base.exists()) {
            base.mkdirs();
        }
        File resourcesDirectory = new File(base, "resources");
        if (!resourcesDirectory.exists()) {
            resourcesDirectory.mkdirs();
        }
        File resourcesFolder = new File(resourcesDirectory, version.getResources());
        if (!resourcesFolder.exists()) {
            resourcesFolder.mkdirs();
        }
        File deploymentsDirectory = new File(base, "deployments");
        if (!deploymentsDirectory.exists()) {
            deploymentsDirectory.mkdirs();
        }
        File deploymentFolder = new File(deploymentsDirectory, version.getDeployment());
        if (!deploymentFolder.exists()) {
            deploymentFolder.mkdirs();
        }

        File binDirectory = new File(deploymentFolder, "bin");
        File nativeDirectory = new File(binDirectory, "natives");
        File jarMods = new File(deploymentFolder, "jarmods");
        if (!jarMods.exists()) {
            jarMods.mkdirs();
        }
        File modDirectory = new File(deploymentFolder, "mods");
        File configDirectory = new File(deploymentFolder, "config");

        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "Verifying Version.");
        }

        MCLatest mcLatest = null;
        MCVersion mcVersion = null;
        String versionsFileName = "versions.json";
        File versionsFile = new File(tmp, versionsFileName);
        JSON versionListJSON;
        
        if (versionsFile.exists()) {
            FileInputStream fileInputStream = new FileInputStream(versionsFile);
            versionListJSON = JSON.parse(fileInputStream);
        } else {
            URL versionListURL = new URL("http://s3.amazonaws.com/Minecraft.Download/versions/versions.json");
            String versionListString = IOUtils.toString(versionListURL.openStream());
            FileUtils.writeStringToFile(versionsFile, versionListString);
            versionListJSON = JSON.parse(versionListString);
        }
        try {
            JPath<?> mcLatestPath = JPath.parse("/latest");
            Value latestValue = mcLatestPath.evaluate(versionListJSON);
            if (latestValue != null) {
                mcLatest = (MCLatest) JSONMarshaler.marshalJSON(latestValue, MCLatest.class);
            } else {
                if (LOGGER.isLoggable(Level.SEVERE)) {
                    LOGGER.log(Level.SEVERE, "No Latest Version: {0}", version);
                }
            }

            JPath<?> mcVersionPath = JPath.parse("/versions[?(@.id=\"" + versionName + "\")]");
            Value versionValue = mcVersionPath.evaluate(versionListJSON);
            if (versionValue != null) {
                JSONMarshaler.registerCodec(Date.class, DateJSONValueCodec.class);
                mcVersion = (MCVersion) JSONMarshaler.marshalJSON(versionValue, MCVersion.class);
            } else {
                if (LOGGER.isLoggable(Level.SEVERE)) {
                    LOGGER.log(Level.SEVERE, "Mojang says version does not exist: {0}", version);
                }
            }
        } catch (ParserException pe) {
            if (LOGGER.isLoggable(Level.SEVERE)) {
                LOGGER.log(Level.SEVERE, "Error Querying Version {0}", pe);
            }
        }
        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "Found Version: {0} Type: {1} Release: {2}", new Object[]{mcVersion.getId(), mcVersion.getType(), mcVersion.getReleaseTime()});
        }

        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "Checking Libraries.");
        }

        JSON versionJSON = null;
        String versionFileName = String.format("libraries-%s.json", versionName);
        File versionFile = new File(tmp, versionFileName);
        if (versionFile.exists()) {
            FileInputStream fileInputStream = new FileInputStream(versionFile);
            versionJSON = JSON.parse(fileInputStream);
        } else {
            URL versionURL = new URL(String.format("http://s3.amazonaws.com/Minecraft.Download/versions/%s/%s.json", versionName, versionName));
            String versionString = IOUtils.toString(versionURL.openStream());
            FileUtils.writeStringToFile(versionFile, versionString);
            versionJSON = JSON.parse(versionString);
        }

        try {
            mcVersion = (MCVersion) JSONMarshaler.marshalJSON(versionJSON, MCVersion.class);
            MCLibrary[] libraries = mcVersion.getLibraries();
            if (libraries != null) {
                for (MCLibrary library : libraries) {
                    if (LOGGER.isLoggable(Level.INFO)) {
                        LOGGER.log(Level.INFO, "Checking {0}", library.getLibraryName());
                    }
                }
            } else {
                if (LOGGER.isLoggable(Level.SEVERE)) {
                    LOGGER.log(Level.SEVERE, "No Libraries Found in response!");
                }
            }

        } catch (ParserException pe) {
            if (LOGGER.isLoggable(Level.SEVERE)) {
                LOGGER.log(Level.SEVERE, "Error Querying Version {0}", pe);
            }
        }

        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "Checking Resources.");
        }

        String resourceName = null;
        if (mcVersion.getId().equals(mcLatest.getLatest()) || mcVersion.getId().equals(mcLatest.getSnapshot())) {
            resourceName = mcVersion.getId();
        } else {
            resourceName = "legacy";
        }


        JSON resourceJSON = null;
        String resourceFileName = String.format("resources-%s.json", versionName);
        File resourceFile = new File(tmp, resourceFileName);
        if (resourceFile.exists()) {
            FileInputStream fileInputStream = new FileInputStream(resourceFile);
            resourceJSON = JSON.parse(fileInputStream);
        } else {
            URL resourceURL = new URL(String.format("https://s3.amazonaws.com/Minecraft.Download/indexes/%s.json", resourceName));
            String resourceString = IOUtils.toString(resourceURL.openStream());
            FileUtils.writeStringToFile(resourceFile, resourceString);
            resourceJSON = JSON.parse(resourceString);
        }

        MCResources mcResources = null;
        try {
            mcResources = (MCResources) JSONMarshaler.marshalJSON(resourceJSON, MCResources.class);

        } catch (ParserException pe) {
            if (LOGGER.isLoggable(Level.SEVERE)) {
                LOGGER.log(Level.SEVERE, "Error Querying Version {0}", pe);
            }
        }

        Set<DownloadMeta> downloadSet = new HashSet<DownloadMeta>();
        String baseResourceURL = "http://resources.download.minecraft.net/%s/%s";
        if (mcResources != null) {
            int downloadSize = 0;

            Map<String, MCResource> resourceMap = mcResources.getObjects();
            Iterator<String> resourceIterator = resourceMap.keySet().iterator();
            while (resourceIterator.hasNext()) {
                String resource = resourceIterator.next();
                MCResource resourceMeta = resourceMap.get(resource);
                File resourceFileHandle = new File(resourcesFolder, resource);
                if (resourceFileHandle.exists()) {
                    try {
                        FileInputStream fis = new FileInputStream(resourceFileHandle);
                        String digest = DigestUtils.sha1Hex(fis);
                        if (!digest.equals(resourceMeta.getHash())) {
                            String resourceURL = String.format(baseResourceURL, resourceMeta.getHash().subSequence(0, 2), resourceMeta.getHash());

                            downloadSet.add(new DownloadMeta(new URL(resourceURL), resourceFileHandle));
                            downloadSize += resourceMeta.getSize();
                            if (LOGGER.isLoggable(Level.INFO)) {
                                LOGGER.log(Level.INFO, "Update Required For {0} {1} {2}", new Object[]{resourceFileHandle.getAbsoluteFile(), resourceMeta.getHash(), digest});
                            }
                        }
                    } catch (Exception e) {
                    }
                } else {
                    String resourceURL = String.format(baseResourceURL, resourceMeta.getHash().subSequence(0, 2), resourceMeta.getHash());

                    downloadSet.add(new DownloadMeta(new URL(resourceURL), resourceFileHandle));
                    downloadSize += resourceMeta.getSize();
                    if (LOGGER.isLoggable(Level.INFO)) {
                        LOGGER.log(Level.INFO, "New Resource Required For {0} {1}", new Object[]{resourceFileHandle.getAbsoluteFile(), resourceMeta.getHash()});
                    }
                }
            }
            if (downloadSize != 0) {
                if (LOGGER.isLoggable(Level.INFO)) {
                    LOGGER.log(Level.INFO, "Download Size {0}", new Object[]{downloadSize});
                }
            }


            for (DownloadMeta downloadMeta : downloadSet) {
                
                if (LOGGER.isLoggable(Level.INFO)) {
                    LOGGER.log(Level.INFO, "Download {0} to {1}", new Object[]{downloadMeta.getUrl(), downloadMeta.getDestination()});
                }
                
                FileUtils.copyURLToFile(downloadMeta.getUrl(), downloadMeta.getDestination());
            }


        }

        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "Checking Mods.");
        }
    }
}
