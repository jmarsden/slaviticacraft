/**
 * Copyright (C) 2014 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package net.slavitica.slaviticacraft.client.console;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConsoleMessageParser {

    public static final Pattern MESSAGE_PATTERN_1;
    public static final Pattern MESSAGE_PATTERN_2;

    static {
        MESSAGE_PATTERN_1 = Pattern.compile("(\\d{4})-(\\d{2})-(\\d{2}) (\\d{2}):(\\d{2}):(\\d{2}) \\[([a-zA-Z0-9]*)\\] \\[([^]]*)\\] (.*)");
        MESSAGE_PATTERN_2 = Pattern.compile("(\\d{4})-(\\d{2})-(\\d{2}) (\\d{2}):(\\d{2}):(\\d{2}) \\[([a-zA-Z0-9]*)\\] \\[([a-zA-Z0-9_]*)\\] (.*)");
    }

    public static ConsoleMessage parseMessage(String message) {

        Matcher matcher = MESSAGE_PATTERN_1.matcher(message);

        if (matcher.matches()) {

            String payload = matcher.group(0);
            int date = Integer.parseInt(matcher.group(3));
            int month = Integer.parseInt(matcher.group(2));
            int year = Integer.parseInt(matcher.group(1));
            int hourOfDay = Integer.parseInt(matcher.group(4));
            int minute = Integer.parseInt(matcher.group(5));
            int second = Integer.parseInt(matcher.group(6));

            Calendar calendar = GregorianCalendar.getInstance();
            calendar.set(year, month - 1, date, hourOfDay, minute, second);

            Date messageDate = calendar.getTime();

            String type = matcher.group(7);
            String extraType = matcher.group(8);
            String messageString = matcher.group(9);

            return new GeneralConsoleMessage(messageDate, payload, type, extraType, messageString);
        }


        return null;
    }
}
