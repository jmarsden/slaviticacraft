/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.slavitica.slaviticacraft.client.console;

import java.util.Date;

/**
 *
 * @author John
 */
public class GeneralConsoleMessage extends ConsoleMessage {

    protected final String channel;
    protected final String mod;

    public GeneralConsoleMessage(Date date, String payload, String channel, String mod, String message) {
        super(date, payload);
        this.message = message;
        this.channel = channel;
        this.mod = mod;
    }

    @Override
    public String toConsoleOutput() {
        StringBuilder outputBuilder = new StringBuilder();
        outputBuilder.append("<span class='sep'>&lt;</span>");
        outputBuilder.append(formatDate(date));
        outputBuilder.append("<span class='sep'>&gt;</span> ");

        outputBuilder.append(formatChannel(channel));
        outputBuilder.append(formatMod(mod)).append(' ');

        outputBuilder.append(this.message);
        return outputBuilder.toString();
    }

    public static String formatMod(String mod) {
        if (mod.equals("STDOUT")) {
            return "";
        } else {
            return "<span class='sep'>[</span><span class='mod'>" + mod + "</span><span class='sep'>]</span>";
        }
    }

    public static String formatChannel(String channel) {
        if (channel == null) {
            return "<span class='sep'>[</span><span class='null'>n</span><span class='sep'>]</span>";
        }
        if (channel.equals("INFO")) {
            return "<span class='sep'>[</span><span class='info'>I</span><span class='sep'>]</span>";
        } else if (channel.equals("WARNING")) {
            return "<span class='sep'>[</span><span class='warn'>W</span><span class='sep'>]</span>";
        } else if (channel.equals("ERROR")) {
            return "<span class='sep'>[</span><span class='error'>E</span><span class='sep'>]</span>";
        } else if (channel.equals("DEBUG")) {
            return "<span class='sep'>[</span><span class='debug'>D</span><span class='sep'>]</span>";
        } else if (channel.equals("SEVERE")) {
            return "<span class='sep'>[</span><span class='error'>S</span><span class='sep'>]</span>";
        } else {
            return "[U:" + channel + "]";
        }
    }
}
