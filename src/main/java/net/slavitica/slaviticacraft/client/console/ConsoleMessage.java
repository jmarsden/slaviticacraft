/**
 * Copyright (C) 2014 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package net.slavitica.slaviticacraft.client.console;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public abstract class ConsoleMessage {

    protected final String payload;
    protected final Date date;
    protected String message;

    public ConsoleMessage(Date date, String payload) {
        this.date = date;
        this.payload = payload;
        this.message = payload;
    }

    public ConsoleMessage(Date date, String payload, String message) {
        this.date = date;
        this.payload = payload;
        this.message = message;
    }

    public String getPayload() {
        return payload;
    }

    public Date getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "" + date + ":" + message;
    }

    public abstract String toConsoleOutput();

    public static String formatDate(Date date) {
        StringBuilder output = new StringBuilder();
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        output.append("<span class='d'>").append(pad(calendar.get(Calendar.DAY_OF_MONTH))).append("</span>");
        output.append("/");
        output.append("<span class='mon'>").append(getMonth(calendar.get(Calendar.MONTH))).append("</span>");
        output.append(" ");
        output.append("<span class='h'>").append(pad(calendar.get(Calendar.HOUR_OF_DAY))).append("</span>");
        output.append(":");
        output.append("<span class='min'>").append(pad(calendar.get(Calendar.MINUTE))).append("</span>");
        output.append(":");
        output.append("<span class='s'>").append(pad(calendar.get(Calendar.SECOND))).append("</span>");
        return output.toString();
    }

    public static String pad(int value) {

        if (value < 10) {
            return "0" + value;
        } else {
            return "" + value;
        }

    }

    public static String getMonth(int month) {
        switch (month) {
            case 0:
                return "JAN";
            case 1:
                return "FEB";
            case 2:
                return "MAR";
            case 3:
                return "APR";
            case 4:
                return "MAY";
            case 5:
                return "JUN";
            case 6:
                return "JUL";
            case 7:
                return "AUG";
            case 8:
                return "SEP";
            case 9:
                return "OCT";
            case 10:
                return "NOV";
            case 11:
                return "DEC";
            default:
                return "UNKOWN";
        }
    }
}
