/**
 * Copyright (C) 2014 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package net.slavitica.slaviticacraft.client.model;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.marshal.JSONMarshaler;
import cc.plural.jsonij.parser.ParserException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.slavitica.slaviticacraft.Model;
import net.slavitica.slaviticacraft.client.Token;
import net.slavitica.slaviticacraft.client.Version;
import net.slavitica.slaviticacraft.client.console.Console;
import net.slavitica.slaviticacraft.client.console.ConsoleMessage;

public class ClientModel implements Model {

    protected String userName;
    protected Token token;
    /**
     * Main Directory
     */
    protected File baseDirectory;
    /**
     * Configured Minecraft Versions
     */
    protected Version[] versions;
    protected int selectedVersion;
    /**
     * Instance Structure
     */
    protected File deploymentDirectory;
    protected File binDirectory;
    protected File nativeDirectory;
    protected File jarMods;
    protected File modDirectory;
    protected File configDirectory;
    /**
     * Resource Structure
     */
    protected File resourcesDirectory;
    protected final Console console;

    public ClientModel() {
        token = null;
        baseDirectory = null;
        versions = null;
        selectedVersion = -1;

        deploymentDirectory = null;
        binDirectory = null;
        nativeDirectory = null;
        jarMods = null;
        modDirectory = null;
        configDirectory = null;

        resourcesDirectory = null;

        console = new Console();
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public Version[] getVersions() {
        return versions;
    }

    public File getBaseDirectory() {
        return baseDirectory;
    }

    public void setBaseDirectory(File baseDirectory) {
        this.baseDirectory = baseDirectory;
    }

    public void setVersions(Version[] versions) {
        this.versions = versions;
    }

    public int getSelectedVersion() {
        return selectedVersion;
    }

    public void setSelectedVersion(int selectedVersion) {
        this.selectedVersion = selectedVersion;
    }

    public File getResourcesDirectory() {
        return resourcesDirectory;
    }

    public void setResourcesDirectory(File resourcesDirectory) {
        this.resourcesDirectory = resourcesDirectory;
    }

    public File getDeploymentDirectory() {
        return deploymentDirectory;
    }

    public void setDeploymentDirectory(File deploymentDirectory) {
        this.deploymentDirectory = deploymentDirectory;
    }

    public File getBinDirectory() {
        return binDirectory;
    }

    public void setBinDirectory(File binDirectory) {
        this.binDirectory = binDirectory;
    }

    public File getNativeDirectory() {
        return nativeDirectory;
    }

    public void setNativeDirectory(File nativeDirectory) {
        this.nativeDirectory = nativeDirectory;
    }

    public File getJarMods() {
        return jarMods;
    }

    public void setJarMods(File jarMods) {
        this.jarMods = jarMods;
    }

    public File getModDirectory() {
        return modDirectory;
    }

    public void setModDirectory(File modDirectory) {
        this.modDirectory = modDirectory;
    }

    public File getConfigDirectory() {
        return configDirectory;
    }

    public void setConfigDirectory(File configDirectory) {
        this.configDirectory = configDirectory;
    }

    public Console getConsole() {
        return console;
    }

    public void addConsoleMessage(ConsoleMessage message) {
    }

    public void loadConfigurations() {
        if (baseDirectory == null || !baseDirectory.exists()) {
            throw new RuntimeException("Base Directory Does Not Exist.");
        }

        InputStream configInputStream = null;
        String configFileName = "/config.json";
        File configFile = new File(baseDirectory, configFileName);
        if (configFile.exists()) {
            try {
                configInputStream = new FileInputStream(configFile);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ClientModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            configInputStream = getClass().getResourceAsStream(configFileName);
        }

        if(configInputStream == null) {
            throw new RuntimeException("Configuration File Not Found. Get a new version.");
        }
        try {
            JSON configuationJSON = JSON.parse(configInputStream);
            Version[] configuredVersions = (Version[]) JSONMarshaler.marshalJSON(configuationJSON.get("versions"), Version[].class);
            setVersions(configuredVersions);
        } catch (ParserException ex) {
            Logger.getLogger(ClientModel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ClientModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
