package net.slavitica.slaviticacraft.mojang;

import java.util.Date;

public class MCVersion {

    private String id;
    private Date time;
    private Date releaseTime;
    private String type;
    private String minecraftArguments;
    private String mainClass;
    private MCLibrary[] libraries;

    public MCVersion() {
        id = null;
        time = null;
        releaseTime = null;
        type = null;
        libraries = null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Date getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(Date releaseTime) {
        this.releaseTime = releaseTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMinecraftArguments() {
        return minecraftArguments;
    }

    public void setMinecraftArguments(String minecraftArguments) {
        this.minecraftArguments = minecraftArguments;
    }

    public String getMainClass() {
        return mainClass;
    }

    public void setMainClass(String mainClass) {
        this.mainClass = mainClass;
    }

    public MCLibrary[] getLibraries() {
        return libraries;
    }

    public void setLibraries(MCLibrary[] libraries) {
        this.libraries = libraries;
    }
}
