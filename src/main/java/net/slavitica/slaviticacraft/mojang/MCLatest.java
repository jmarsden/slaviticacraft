package net.slavitica.slaviticacraft.mojang;

public class MCLatest {
    
    private String latest;
    private String snapshot;
    
    public MCLatest() {
        latest = null;
        snapshot = null;
    }

    public String getLatest() {
        return latest;
    }

    public void setLatest(String latest) {
        this.latest = latest;
    }

    public String getSnapshot() {
        return snapshot;
    }

    public void setSnapshot(String snapshot) {
        this.snapshot = snapshot;
    }
}
