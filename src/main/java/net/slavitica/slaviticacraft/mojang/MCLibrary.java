package net.slavitica.slaviticacraft.mojang;

import java.util.StringTokenizer;

public class MCLibrary {

    private String name;
    private String libraryPackage;
    private String libraryName;
    private String libraryVersion;

    public MCLibrary() {
        name = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        StringTokenizer tokenizer = new StringTokenizer(name, ":");
        if (tokenizer.countTokens() == 3) {
            libraryPackage = tokenizer.nextToken();
            libraryName = tokenizer.nextToken();
            libraryVersion = tokenizer.nextToken();
        }
    }

    public String getLibraryPackage() {
        return libraryPackage;
    }

    public String getLibraryName() {
        return libraryName;
    }

    public String getLibraryVersion() {
        return libraryVersion;
    }
}
