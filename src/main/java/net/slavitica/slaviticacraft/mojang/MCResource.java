package net.slavitica.slaviticacraft.mojang;

public class MCResource {
    
    private String hash;
    private int size;
    
    public MCResource() {
        hash = null;
        size = -1;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
