package net.slavitica.slaviticacraft.mojang;

import java.util.HashMap;
import java.util.Map;

public class MCResources {
    
    private boolean virtual;
    private Map<String, MCResource> objects;
    
    public MCResources() {
        virtual = false;
        objects = new HashMap<String, MCResource>();
    }

    public boolean isVirtual() {
        return virtual;
    }

    public void setVirtual(boolean virtual) {
        this.virtual = virtual;
    }

    public Map<String, MCResource> getObjects() {
        return objects;
    }

    public void setObjects(Map<String, MCResource> objects) {
        this.objects = objects;
    }
}
