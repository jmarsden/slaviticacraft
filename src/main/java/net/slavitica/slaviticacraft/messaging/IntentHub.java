/**
 * Copyright (C) 2014 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package net.slavitica.slaviticacraft.messaging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IntentHub {
    
    private static IntentHub singleton;
    
    private Map<Class<? extends Intent>, List<IntentReceiver>> listenerRegistry;
    
    static {
        singleton = null;
    }
    
    public IntentHub() {
        listenerRegistry = new HashMap<Class<? extends Intent>, List<IntentReceiver>>();
    }
    
    public void registerForIntent(IntentReceiver listener, Intent intent) {
        registerForIntent(listener, intent.getClass());
    }
    
    public void registerForIntent(IntentReceiver listener, Class<? extends Intent> intentClass) {
        List<IntentReceiver> listenerList = listenerRegistry.get(intentClass);
        if(listenerList == null) {
            listenerList = new ArrayList<IntentReceiver>();
            listenerRegistry.put(intentClass, listenerList);
        }
        if(!listenerList.contains(listener)) {
            listenerList.add(listener);
        }
    }
    
    public void deregisterForIntent(IntentReceiver listener, Class<? extends Intent> intentClass) {
        List<IntentReceiver> listenerList = listenerRegistry.get(intentClass);
        if(listenerList != null && listenerList.contains(listener)) {
            listenerList.remove(listener);
        }
    }
    
    public void notifyIntent(Intent intent) {
        List<IntentReceiver> listenerList = listenerRegistry.get(intent.getClass());
        if(listenerList != null) {
            for(IntentReceiver receiver: listenerList) {
                receiver.receiveIntent(intent);
                intent.addReceiver(receiver);
            }
        }
    }
    
    	
    public static IntentHub lookupSingleton() {
            if(singleton == null) {
                    singleton = new IntentHub();
            }
            return singleton;
    }
}
